#pragma once

#ifdef IN_USERMODE
#include <winternl.h>
#include <winioctl.h>
#else
#include <wdm.h>
#endif

#define DEVICE_NAME L"\\Device\\HidingDriver"

#ifndef IN_USERMODE
UNICODE_STRING gDeviceName = RTL_CONSTANT_STRING(DEVICE_NAME);
#endif

#define IOCTL_HIDEDRV_SET_HIDE					CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_BUFFERED, FILE_ALL_ACCESS)
#define IOCTL_HIDEDRV_UNSET_HIDE				CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_ALL_ACCESS)

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDirModel>
#include <qstringlistmodel.h>
#include <qlistwidget.h>
#include <io.h>

#include <regex>

#include <Windows.h>
#include <Fltuser.h>

#include <winternl.h>
#include <winioctl.h>
#define IN_USERMODE
#include "interface.h"

//#pragma comment (lib, "C:\\Program Files (x86)\\Windows Kits\\8.1\\Lib\\win7\\km\\x86\\fltmgr.lib")

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_treeView_doubleClicked(const QModelIndex &index);

    void on_pushButton_2_clicked();
	void on_treeView_clicked(const QModelIndex &index);

    void on_pushButton_3_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

    void GetFolderName(const QString&, QString&);

	bool IsUnice(QString, QStringList);

    void on_pushButton_clicked();

	void FindF(intptr_t, char[], char[], _finddata_t*);

	void ReversSlash(QString&);

	//int SendIoctl(ULONG IoctlCode, UNICODE_STRING String);

private:
    Ui::MainWindow *ui;
	QDirModel *model;
	QStringListModel* qmList;
	QStringList List;
	QString fPath, fName;
    QModelIndex selFileIndex,selListIndex;
	HANDLE hPort;
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
	qmList(nullptr)
{
    ui->setupUi(this);
    model = new QDirModel(this);
    model->setReadOnly(false);
	this->hPort = INVALID_HANDLE_VALUE;
    // Tie TreeView with DirModel
        // QTreeView::setModel(QAbstractItemModel * model)
        // Reimplemented from QAbstractItemView::setModel().
	ui->treeView->setModel(model);
	model->setSorting(QDir::DirsFirst|QDir::Name|QDir::IgnoreCase);
    //qmList = new QStringListModel(this);
    //ui->listView->setModel(qmList);
}

MainWindow::~MainWindow()
{
	if (ui)
		delete ui;
	if (model)
		delete model;
	if (qmList)
		delete qmList;
}

/*
$(DDK_LIB_PATH)\NtosKrnl.lib;
int MainWindow::SendIoctl(ULONG IoctlCode, UNICODE_STRING String)
{
	NTSTATUS status;
	OBJECT_ATTRIBUTES objectAttributes;
	HANDLE configHandle;
	IO_STATUS_BLOCK ioStatusBlock;
	UNICODE_STRING deviceName;

	RtlInitUnicodeString(&deviceName, DEVICE_NAME);

	InitializeObjectAttributes(
		&objectAttributes,
		&deviceName,
		OBJ_CASE_INSENSITIVE,
		NULL,
		NULL);

	status = NtOpenFile(
		&configHandle,
		FILE_ALL_ACCESS,
		&objectAttributes,
		&ioStatusBlock,
		0,
		FILE_SYNCHRONOUS_IO_NONALERT);
	if (!NT_SUCCESS(status))
	{
		wprintf(L"Failed to open config handle: 0x%x\n", status);
		return -1;
	}

	status = NtDeviceIoControlFile(
		configHandle,
		NULL,
		NULL,
		NULL,
		&ioStatusBlock,
		IoctlCode,
		String.Buffer,
		//String != NULL ? wcslen(String) * sizeof(WCHAR) : 0,
		String.Length,
		NULL,
		0);
	if (!NT_SUCCESS(status))
	{
		wprintf(L"IOCTL failed: 0x%x\n", status);
		return -1;
	}

	return 0;
}
*/

void MainWindow::on_treeView_doubleClicked(const QModelIndex &index)
{
	fName = model->fileName(index);
	fPath = model->filePath(index);
	QString FolderName, time_path;
	GetFolderName(fPath, FolderName);

	if (FolderName == fName)
	{
		if (IsUnice(fPath, List))
		{
			List.push_back(fPath);
			ui->listWidget->addItem(fPath);
		}
	}
	else
	{
		if (IsUnice(fPath + "/" + fName, List))
		{
			time_path = fPath + "/" + fName;
			List.push_back(time_path);
			ui->listWidget->addItem(fPath + "/" + fName);
		}
	}
}


void MainWindow::on_pushButton_2_clicked()
{
	if (!selFileIndex.isValid())
		return;
	fName = model->fileName(selFileIndex);
	fPath = model->filePath(selFileIndex);

	QString FolderName, time_path;
	GetFolderName(fPath, FolderName);

	if (FolderName == fName)
	{
		if (IsUnice(fPath, List))
		{
			List.push_back(fPath);
			ui->listWidget->addItem(fPath);
		}
	}
	else
	{
		if (IsUnice(fPath + "/" + fName, List))
		{
			time_path = fPath + "/" + fName;
			List.push_back(time_path);
			ui->listWidget->addItem(fPath + "/" + fName);
		}
	}
}

void MainWindow::on_treeView_clicked(const QModelIndex &index)
{
    selFileIndex = index;
}

void MainWindow::on_pushButton_3_clicked()
{
    if(!selListIndex.isValid())
        return;
    List.removeAt(selListIndex.row());
    ui->listWidget->removeItemWidget(ui->listWidget->takeItem(selListIndex.row()));
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
    selListIndex = index;
}

void MainWindow::GetFolderName(const QString& fP, QString& fN)
{
/*	int i,j=0;
    QChar tmp;
	for (i = fP.size() - 1; i >= 0,fP[i]!='/'; i--,j++)
	{
		fN[j] = fP[i];
	}
   char str[256];
	for (i = 0; i < fN.size()/2; i++)
	{
        tmp = fN[i];
        fN[i]= fN[fN.size() - i];
        fN[fN.size() - i] = tmp;

        //std::swap(fN[i].cell(),fN[fN.size()-1-i].cell());
    }*/

    const std::string filePath = fP.toLocal8Bit().constData();
    std::string fileName = fN.toLocal8Bit().constData();

    std::string::size_type slashPos = filePath.find_last_of(L'/');
    fileName = filePath.substr(slashPos+1);

    fN = fN.fromLocal8Bit(fileName.c_str());

	return;
}

bool MainWindow::IsUnice(QString str, QStringList fLstr)
{
	int i = 0;
	for (i = 0; i < fLstr.size(); i++)
	{
		if (str == fLstr[i])
			return false;
	}
	return true;
}

void MainWindow::on_pushButton_clicked()
{
	int i, list_len = this->List.size();
	QDir fDir;
	std::string rule_str(ui->lineEdit->text().toUtf8());
	//FilterConnectCommunicationPort(L"HidingDriver", 0, NULL, 0, NULL, &hPort);
	/*\\Device\\*/
	this->hPort = CreateFileW(L"\\\\.\\HidingDriver", 0, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);
	if(rule_str.find("*.*")&&rule_str.size()>3)
		DeviceIoControl(hPort, IOCTL_HIDEDRV_SET_HIDE, &ui->lineEdit->text().toUtf8(), sizeof(&ui->lineEdit->text().toUtf8()), 0, 0, 0, 0);
	if (list_len)
	{
		for (i = 0; i < list_len; i++)
		{
			ReversSlash(List[i]);
			//FilterSendMessage(this->hPort, &List[i].toUtf8(), sizeof(List[i].toUtf8()), NULL, NULL, &dufferReturn);
			DeviceIoControl(hPort, IOCTL_HIDEDRV_SET_HIDE, &List[i].toUtf8(), sizeof(&List[i].toUtf8()), 0, 0, 0, 0);
		}
	}

	if (ui->lineEdit->text().size())
	{
		char path[256] = "", mask[256] = "";

		intptr_t handle;
		_finddata_t* finf = new _finddata_t;

		handle = _findfirst(ui->lineEdit->text().toUtf8().constData(), finf);

		if (handle)
			FindF(handle, path, mask, finf);

		_findclose(handle);
	}
}


void MainWindow::FindF(intptr_t handle, char path[], char mask[], _finddata_t* fdat)
{
	char path_mask[256] = "", _path[256] = "",temp[256]="";
	//regex reg(mask);
	DWORD dufferReturn = 0;
	intptr_t _handle = handle;
	//*if (fdat->attrib==16)
	//{
	if (strcmp(fdat->name, ".") == 0)
		_findnext(handle, fdat);
	if (strcmp(fdat->name, "..") == 0)
		_findnext(handle, fdat);
	while (fdat->attrib == 22 || fdat->attrib == 18)
		_findnext(handle, fdat);
	if ((fdat->attrib == 16/*|| fdat->attrib == 18 || fdat->attrib == 22*/) && strcmp(fdat->name, "..") != 0)
	{
		strcat(_path, path);
		//if (regex_match(fdat->name, reg))
		strcpy(temp, path);
		strcat(temp, "\\");
		strcat(temp, fdat->name);

		//FilterSendMessage(this->hPort, temp, sizeof(temp), NULL, NULL, &dufferReturn);
		strcat(_path, fdat->name);
		strcpy(path_mask, _path);
		strcat(path_mask, "\\");
		strcat(_path, "\\");
		strcat(path_mask, mask);
		handle = _findfirst(path_mask, fdat);
		if (handle)
			FindF(handle, _path, mask, fdat);
	}
	else
	{
		//if (regex_match(fdat->name, reg) && strcmp(fdat->name, "..") != 0 && _handle != -1)
		strcpy(temp, path);
		strcat(temp, "\\");
		strcat(temp, fdat->name);

		//FilterSendMessage(this->hPort, temp, sizeof(temp), NULL, NULL, &dufferReturn);
	}
	strcpy(_path, path);
	while ((_handle != -1 && !_findnext(_handle, fdat)))
	{
		while (fdat->attrib == 22 || fdat->attrib == 18)
			_findnext(handle, fdat);
		if ((fdat->attrib == 16 /*|| fdat->attrib == 18 || fdat->attrib == 22*/) && strcmp(fdat->name, "..") != 0)
		{
			strcpy(_path, path);
			//if (regex_match(fdat->name, reg))
			strcpy(temp, path);
			strcat(temp, "\\");
			strcat(temp, fdat->name);

			FilterSendMessage(this->hPort, temp, sizeof(temp), NULL, NULL, &dufferReturn);
			strcat(_path, fdat->name);
			strcpy(path_mask, _path);
			strcat(path_mask, "\\");
			strcat(_path, "\\");
			strcat(path_mask, mask);
			handle = _findfirst(path_mask, fdat);
			if (handle)
				FindF(handle, _path, mask, fdat);
		}
		else
		{
			//if (regex_match(fdat->name, reg))
			strcpy(temp, path);
			strcat(temp, "\\");
			strcat(temp, fdat->name);

			FilterSendMessage(this->hPort, temp, sizeof(temp), NULL, NULL, &dufferReturn);
		}
	}

}

void MainWindow::ReversSlash(QString& path)
{
	int i, path_len = path.length();
	for (i = 0; i < path_len; i++)
	{
		if (path[i] == '/')
		{
			path[i] = '\\';
			path.insert(i + 1, '\\');
		}
	}

}
#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")

#include "interface.h"
#define FILE_NAME_TAG 'nFlC'

PFLT_FILTER gFilterHandle;
PDEVICE_OBJECT gDeviceObject;
ULONG_PTR OperationStatusCtx = 1;

PUNICODE_STRING gHideFileName;


#define PTDBG_TRACE_ROUTINES            0x00000001
#define PTDBG_TRACE_OPERATION_STATUS    0x00000002

ULONG gTraceFlags = 0;


#define PT_DBG_PRINT( _dbgLevel, _string )          \
    (FlagOn(gTraceFlags,(_dbgLevel)) ?              \
        DbgPrint _string :                          \
        ((int)0))


/*************************************************************************
Prototypes
*************************************************************************/

EXTERN_C_START

DRIVER_INITIALIZE DriverEntry;
NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
);

NTSTATUS
FsFilter1InstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
);

VOID
FsFilter1InstanceTeardownStart(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_TEARDOWN_FLAGS Flags
);

VOID
FsFilter1InstanceTeardownComplete(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_TEARDOWN_FLAGS Flags
);

NTSTATUS
FsFilter1Unload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
);

NTSTATUS
FsFilter1InstanceQueryTeardown(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
FsFilter1PreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

VOID
FsFilter1OperationStatusCallback(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ PFLT_IO_PARAMETER_BLOCK ParameterSnapshot,
	_In_ NTSTATUS OperationStatus,
	_In_ PVOID RequesterContext
);

FLT_POSTOP_CALLBACK_STATUS
FsFilter1PostOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_opt_ PVOID CompletionContext,
	_In_ FLT_POST_OPERATION_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
FsFilter1PreOperationNoPostOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
);

BOOLEAN
FsFilter1DoRequestOperationStatus(
	_In_ PFLT_CALLBACK_DATA Data
);


NTSTATUS
HideDrvIoctl(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
);

NTSTATUS
HideDrvCreateClose(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
);

EXTERN_C_END

//
//  Assign text sections for each routine.
//

#ifdef ALLOC_PRAGMA
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, FsFilter1Unload)
#pragma alloc_text(PAGE, FsFilter1InstanceQueryTeardown)
#pragma alloc_text(PAGE, FsFilter1InstanceSetup)
#pragma alloc_text(PAGE, FsFilter1InstanceTeardownStart)
#pragma alloc_text(PAGE, FsFilter1InstanceTeardownComplete)
#endif

//
//  operation registration
//

CONST FLT_OPERATION_REGISTRATION Callbacks[] = {

	/*    { IRP_MJ_CREATE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },*/

	/*    { IRP_MJ_CREATE_NAMED_PIPE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_CLOSE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_READ,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },
	*/
	/*   { IRP_MJ_WRITE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },*/
	/*
	{ IRP_MJ_QUERY_INFORMATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SET_INFORMATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_QUERY_EA,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SET_EA,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_FLUSH_BUFFERS,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_QUERY_VOLUME_INFORMATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SET_VOLUME_INFORMATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },*/

	{ IRP_MJ_DIRECTORY_CONTROL,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },
	/*
	{ IRP_MJ_FILE_SYSTEM_CONTROL,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_DEVICE_CONTROL,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_INTERNAL_DEVICE_CONTROL,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SHUTDOWN,
	0,
	FsFilter1PreOperationNoPostOperation,
	NULL },                               //post operations not supported

	{ IRP_MJ_LOCK_CONTROL,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_CLEANUP,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_CREATE_MAILSLOT,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_QUERY_SECURITY,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SET_SECURITY,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_QUERY_QUOTA,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_SET_QUOTA,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_PNP,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_ACQUIRE_FOR_SECTION_SYNCHRONIZATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_RELEASE_FOR_SECTION_SYNCHRONIZATION,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_ACQUIRE_FOR_MOD_WRITE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_RELEASE_FOR_MOD_WRITE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_ACQUIRE_FOR_CC_FLUSH,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_RELEASE_FOR_CC_FLUSH,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_FAST_IO_CHECK_IF_POSSIBLE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_NETWORK_QUERY_OPEN,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_MDL_READ,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_MDL_READ_COMPLETE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_PREPARE_MDL_WRITE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_MDL_WRITE_COMPLETE,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_VOLUME_MOUNT,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },

	{ IRP_MJ_VOLUME_DISMOUNT,
	0,
	FsFilter1PreOperation,
	FsFilter1PostOperation },
	*/

	{ IRP_MJ_OPERATION_END }
};

//
//  This defines what we want to filter with FltMgr
//

CONST FLT_REGISTRATION FilterRegistration = {

	sizeof(FLT_REGISTRATION),         //  Size
	FLT_REGISTRATION_VERSION,           //  Version
	0,                                  //  Flags

	NULL,                               //  Context
	Callbacks,                          //  Operation callbacks

	FsFilter1Unload,                           //  MiniFilterUnload

	FsFilter1InstanceSetup,                    //  InstanceSetup
	NULL, //FsFilter1InstanceQueryTeardown,            //  InstanceQueryTeardown
	NULL, //FsFilter1InstanceTeardownStart,            //  InstanceTeardownStart
	NULL, //FsFilter1InstanceTeardownComplete,         //  InstanceTeardownComplete

	NULL,                               //  GenerateFileName
	NULL,                               //  GenerateDestinationFileName
	NULL                                //  NormalizeNameComponent

};



NTSTATUS
FsFilter1InstanceSetup(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_SETUP_FLAGS Flags,
	_In_ DEVICE_TYPE VolumeDeviceType,
	_In_ FLT_FILESYSTEM_TYPE VolumeFilesystemType
)
/*++

Routine Description:

This routine is called whenever a new instance is created on a volume. This
gives us a chance to decide if we need to attach to this volume or not.

If this routine is not defined in the registration structure, automatic
instances are always created.

Arguments:

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance and its associated volume.

Flags - Flags describing the reason for this attach request.

Return Value:

STATUS_SUCCESS - attach
STATUS_FLT_DO_NOT_ATTACH - do not attach

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);
	UNREFERENCED_PARAMETER(VolumeDeviceType);
	UNREFERENCED_PARAMETER(VolumeFilesystemType);

	PAGED_CODE();

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1InstanceSetup: Entered\n"));

	return STATUS_SUCCESS;
}


NTSTATUS
FsFilter1InstanceQueryTeardown(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
)
/*++

Routine Description:

This is called when an instance is being manually deleted by a
call to FltDetachVolume or FilterDetach thereby giving us a
chance to fail that detach request.

If this routine is not defined in the registration structure, explicit
detach requests via FltDetachVolume or FilterDetach will always be
failed.

Arguments:

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance and its associated volume.

Flags - Indicating where this detach request came from.

Return Value:

Returns the status of this operation.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1InstanceQueryTeardown: Entered\n"));

	return STATUS_SUCCESS;
}


VOID
FsFilter1InstanceTeardownStart(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_TEARDOWN_FLAGS Flags
)
/*++

Routine Description:

This routine is called at the start of instance teardown.

Arguments:

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance and its associated volume.

Flags - Reason why this instance is being deleted.

Return Value:

None.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1InstanceTeardownStart: Entered\n"));
}


VOID
FsFilter1InstanceTeardownComplete(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_TEARDOWN_FLAGS Flags
)
/*++

Routine Description:

This routine is called at the end of instance teardown.

Arguments:

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance and its associated volume.

Flags - Reason why this instance is being deleted.

Return Value:

None.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1InstanceTeardownComplete: Entered\n"));
}


/*************************************************************************
MiniFilter initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)

{
	NTSTATUS status;

	UNREFERENCED_PARAMETER(RegistryPath);

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!DriverEntry: Entered\n"));

	//
	//  Register with FltMgr to tell it our callback routines
	//

	status = FltRegisterFilter(DriverObject,
		&FilterRegistration,
		&gFilterHandle);

	FLT_ASSERT(NT_SUCCESS(status));

	if (NT_SUCCESS(status)) {

		//
		//  Start filtering i/o
		//

		status = FltStartFiltering(gFilterHandle);

		if (!NT_SUCCESS(status)) {

			FltUnregisterFilter(gFilterHandle);
		}
	}

	gHideFileName = NULL;
	// Create the device object used to configure the filter
	//
	status = IoCreateDevice(
		DriverObject,
		0,
		&gDeviceName,
		FILE_DEVICE_UNKNOWN,
		FILE_DEVICE_SECURE_OPEN,
		FALSE,
		&gDeviceObject);
	if (!NT_SUCCESS(status))
	{
		//
		// If this doesn't work, unregister the filter and fail driver load
		//
		FltUnregisterFilter(gFilterHandle);
		return status;
	}

	//
	// Install our dispatch handler for IOCTL requests
	//
	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = HideDrvIoctl;
	// Install our dispatch handler for creates and closes
	//
	DriverObject->MajorFunction[IRP_MJ_CREATE] = HideDrvCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = HideDrvCreateClose;

	DbgPrint("MiniFilter: Started.");
	return status;
}

NTSTATUS
FsFilter1Unload(
	_In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
/*++

Routine Description:

This is the unload routine for this miniFilter driver. This is called
when the minifilter is about to be unloaded. We can fail this unload
request if this is not a mandatory unload indicated by the Flags
parameter.

Arguments:

Flags - Indicating if this is a mandatory unload.

Return Value:

Returns STATUS_SUCCESS.

--*/
{
	UNREFERENCED_PARAMETER(Flags);

	PAGED_CODE();

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1Unload: Entered\n"));

	FltUnregisterFilter(gFilterHandle);
	IoDeleteDevice(gDeviceObject);

	//
	// We need to cleanup our file name allocations or we'll leak memory.

	if (gHideFileName != NULL)
	{
		ExFreePoolWithTag(gHideFileName, FILE_NAME_TAG);
	}

	return STATUS_SUCCESS;
}


NTSTATUS
HideDrvCreateClose(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	//
	// We don't maintain any handle-specific state here so it's a nop
	//
	Irp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

NTSTATUS
HideDrvIoctl(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	PIO_STACK_LOCATION IrpSp = IoGetCurrentIrpStackLocation(Irp);
	BOOLEAN unset;
	PUNICODE_STRING *target, *target2;
	UNICODE_STRING source;
	ULONG allocationSize;

	DBG_UNREFERENCED_PARAMETER(DeviceObject);
	NT_ASSERT(DeviceObject == gDeviceObject);
	NT_ASSERT(IrpSp->MajorFunction == IRP_MJ_DEVICE_CONTROL);

	//
	// Handle each operation using lots of shared code
	//
	switch (IrpSp->Parameters.DeviceIoControl.IoControlCode)
	{
	case IOCTL_HIDEDRV_SET_HIDE:
		unset = FALSE;
		target = &gHideFileName;
		target2 = NULL;
		break;

	case IOCTL_HIDEDRV_UNSET_HIDE:
		unset = TRUE;
		target = &gHideFileName;
		target2 = NULL;
		break;

	default:
		//
		// Anything else is not supported
		//
		Irp->IoStatus.Status = STATUS_NOT_SUPPORTED;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return STATUS_NOT_SUPPORTED;
	}

	//
	// Always "unset" the target(s)
	//
	if ((target != NULL) && (*target != NULL))
	{
		ExFreePoolWithTag(*target, FILE_NAME_TAG);
		*target = NULL;
	}
	if ((target2 != NULL) && (*target2 != NULL))
	{
		ExFreePoolWithTag(*target2, FILE_NAME_TAG);
		*target2 = NULL;
	}

	//
	// If we're doing a set also, copy the string from the input buffer
	//
	if (unset == FALSE)
	{
		//
		// Setup the source string from the input buffer
		//
		source.Buffer = (PWCHAR)Irp->AssociatedIrp.SystemBuffer;
		source.Length = (USHORT)IrpSp->Parameters.DeviceIoControl.InputBufferLength;
		source.MaximumLength = source.Length;

		//
		// We'll use one allocation for both the string header and the string data
		//
		allocationSize = sizeof(UNICODE_STRING) + source.MaximumLength;
		*target = (PUNICODE_STRING)ExAllocatePoolWithTag(NonPagedPool, allocationSize, FILE_NAME_TAG);
		if (*target == NULL)
		{
			Irp->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
			IoCompleteRequest(Irp, IO_NO_INCREMENT);
			return STATUS_INSUFFICIENT_RESOURCES;
		}

		//
		// Put the header first, then the string data after
		//
		(*target)->Buffer = (PWCHAR)((PUCHAR)(*target) + sizeof(UNICODE_STRING));
		(*target)->MaximumLength = source.MaximumLength;
		(*target)->Length = 0;

		//
		// Copy their data into our string and upcase it so the FsRtl API we use works as expected
		//
		NT_VERIFY(NT_SUCCESS(RtlUpcaseUnicodeString(*target, &source, FALSE)));
	}

	//
	// Success!
	//
	Irp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}


/*************************************************************************
MiniFilter callback routines.
*************************************************************************/
FLT_PREOP_CALLBACK_STATUS
FsFilter1PreOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)

{
	//    NTSTATUS status;

	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1PreOperation: Entered\n"));


	if (FLT_IS_FS_FILTER_OPERATION(Data))
	{
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	// Check if we have a file name configured to hide
	//
	if (gHideFileName == NULL)
	{
		//
		// Nothing to hide, so just let them through
		//
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	//
	// Check if it's a query directory request
	//
	if (Data->Iopb->MinorFunction != IRP_MN_QUERY_DIRECTORY)
	{
		//
		// Not a query directory request, so let it through
		//
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}


	// Check for a directory enumeration
	//
	switch (Data->Iopb->Parameters.DirectoryControl.QueryDirectory.FileInformationClass)
	{
	case FileIdFullDirectoryInformation:
	case FileIdBothDirectoryInformation:
	case FileBothDirectoryInformation:
	case FileDirectoryInformation:
	case FileFullDirectoryInformation:
	case FileNamesInformation:
		//
		// These are all enumerations we want to intercept
		//
		break;

	default:
		//
		// Anything else is not
		//
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	// For this one, we do want a post-operation callback so we can examine what
	// the directory enumeration is returning and make modifications if required.
	//
	return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}



VOID
FsFilter1OperationStatusCallback(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ PFLT_IO_PARAMETER_BLOCK ParameterSnapshot,
	_In_ NTSTATUS OperationStatus,
	_In_ PVOID RequesterContext
)
/*++

Routine Description:

This routine is called when the given operation returns from the call
to IoCallDriver.  This is useful for operations where STATUS_PENDING
means the operation was successfully queued.  This is useful for OpLocks
and directory change notification operations.

This callback is called in the context of the originating thread and will
never be called at DPC level.  The file object has been correctly
referenced so that you can access it.  It will be automatically
dereferenced upon return.

This is non-pageable because it could be called on the paging path

Arguments:

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance, its associated volume and
file object.

RequesterContext - The context for the completion routine for this
operation.

OperationStatus -

Return Value:

The return value is the status of the operation.

--*/
{
	UNREFERENCED_PARAMETER(FltObjects);

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1OperationStatusCallback: Entered\n"));

	PT_DBG_PRINT(PTDBG_TRACE_OPERATION_STATUS,
		("FsFilter1!FsFilter1OperationStatusCallback: Status=%08x ctx=%p IrpMj=%02x.%02x \"%s\"\n",
			OperationStatus,
			RequesterContext,
			ParameterSnapshot->MajorFunction,
			ParameterSnapshot->MinorFunction,
			FltGetIrpName(ParameterSnapshot->MajorFunction)));
}


FLT_POSTOP_CALLBACK_STATUS
FsFilter1PostOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_opt_ PVOID CompletionContext,
	_In_ FLT_POST_OPERATION_FLAGS Flags
)

{
	PFILE_DIRECTORY_INFORMATION fileDirInfo, lastFileDirInfo, nextFileDirInfo;
	PFILE_FULL_DIR_INFORMATION fileFullDirInfo, lastFileFullDirInfo, nextFileFullDirInfo;
	PFILE_NAMES_INFORMATION fileNamesInfo, lastFileNamesInfo, nextFileNamesInfo;
	PFILE_BOTH_DIR_INFORMATION fileBothDirInfo, lastFileBothDirInfo, nextFileBothDirInfo;
	PFILE_ID_BOTH_DIR_INFORMATION fileIdBothDirInfo, lastFileIdBothDirInfo, nextFileIdBothDirInfo;
	PFILE_ID_FULL_DIR_INFORMATION fileIdFullDirInfo, lastFileIdFullDirInfo, nextFileIdFullDirInfo;
	UNICODE_STRING fileName;
	ULONG moveLength;

	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);
	UNREFERENCED_PARAMETER(Flags);



	//
	// There's a chance that this was removed after the pre-callback
	//
	if (&gHideFileName == NULL)
	{
		//
		// It's gone now.
		//
		return FLT_POSTOP_FINISHED_PROCESSING;
	}
	else DbgPrint("Something....");

	//
	// If the operation didn't succeed, we don't care
	//
	if (!NT_SUCCESS(Data->IoStatus.Status))
	{
		//
		// Nothing to fixup but it failed
		//
		return FLT_POSTOP_FINISHED_PROCESSING;
	}
	switch (Data->Iopb->Parameters.DirectoryControl.QueryDirectory.FileInformationClass)
	{
	case FileDirectoryInformation:
		lastFileDirInfo = NULL;
		fileDirInfo = (PFILE_DIRECTORY_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileDirInfo->FileName;
			fileName.Length = (USHORT)fileDirInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileDirInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileDirInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileDirInfo->NextEntryOffset += fileDirInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileDirInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileDirInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileDirInfo = (PFILE_DIRECTORY_INFORMATION)((PUCHAR)fileDirInfo + fileDirInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileDirInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_DIRECTORY_INFORMATION, FileName) + nextFileDirInfo->FileNameLength;
							nextFileDirInfo = (PFILE_DIRECTORY_INFORMATION)((PUCHAR)nextFileDirInfo + nextFileDirInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_DIRECTORY_INFORMATION, FileName) + nextFileDirInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileDirInfo,
							(PUCHAR)fileDirInfo + fileDirInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileDirInfo = fileDirInfo;
			fileDirInfo = (PFILE_DIRECTORY_INFORMATION)((PUCHAR)fileDirInfo + fileDirInfo->NextEntryOffset);
			if (lastFileDirInfo == fileDirInfo)
			{
				break;
			}
		}
		break;

	case FileFullDirectoryInformation:
		lastFileFullDirInfo = NULL;
		fileFullDirInfo = (PFILE_FULL_DIR_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileFullDirInfo->FileName;
			fileName.Length = (USHORT)fileFullDirInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileFullDirInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileFullDirInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileFullDirInfo->NextEntryOffset += fileFullDirInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileFullDirInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileFullDirInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileFullDirInfo = (PFILE_FULL_DIR_INFORMATION)((PUCHAR)fileFullDirInfo + fileFullDirInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileFullDirInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName) + nextFileFullDirInfo->FileNameLength;
							nextFileFullDirInfo = (PFILE_FULL_DIR_INFORMATION)((PUCHAR)nextFileFullDirInfo + nextFileFullDirInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName) + nextFileFullDirInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileFullDirInfo,
							(PUCHAR)fileFullDirInfo + fileFullDirInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileFullDirInfo = fileFullDirInfo;
			fileFullDirInfo = (PFILE_FULL_DIR_INFORMATION)((PUCHAR)fileFullDirInfo + fileFullDirInfo->NextEntryOffset);
			if (lastFileFullDirInfo == fileFullDirInfo)
			{
				break;
			}
		}
		break;

	case FileNamesInformation:
		lastFileNamesInfo = NULL;
		fileNamesInfo = (PFILE_NAMES_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileNamesInfo->FileName;
			fileName.Length = (USHORT)fileNamesInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileNamesInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileNamesInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileNamesInfo->NextEntryOffset += fileNamesInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileNamesInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileNamesInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileNamesInfo = (PFILE_NAMES_INFORMATION)((PUCHAR)fileNamesInfo + fileNamesInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileNamesInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_NAMES_INFORMATION, FileName) + nextFileNamesInfo->FileNameLength;
							nextFileNamesInfo = (PFILE_NAMES_INFORMATION)((PUCHAR)nextFileNamesInfo + nextFileNamesInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_NAMES_INFORMATION, FileName) + nextFileNamesInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileNamesInfo,
							(PUCHAR)fileNamesInfo + fileNamesInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileNamesInfo = fileNamesInfo;
			fileNamesInfo = (PFILE_NAMES_INFORMATION)((PUCHAR)fileNamesInfo + fileNamesInfo->NextEntryOffset);
			if (lastFileNamesInfo == fileNamesInfo)
			{
				break;
			}
		}
		break;

	case FileBothDirectoryInformation:
		lastFileBothDirInfo = NULL;
		fileBothDirInfo = (PFILE_BOTH_DIR_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileBothDirInfo->FileName;
			fileName.Length = (USHORT)fileBothDirInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileBothDirInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileBothDirInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileBothDirInfo->NextEntryOffset += fileBothDirInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileBothDirInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileBothDirInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileBothDirInfo = (PFILE_BOTH_DIR_INFORMATION)((PUCHAR)fileBothDirInfo + fileBothDirInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileBothDirInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_BOTH_DIR_INFORMATION, FileName) + nextFileBothDirInfo->FileNameLength;
							nextFileBothDirInfo = (PFILE_BOTH_DIR_INFORMATION)((PUCHAR)nextFileBothDirInfo + nextFileBothDirInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_BOTH_DIR_INFORMATION, FileName) + nextFileBothDirInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileBothDirInfo,
							(PUCHAR)fileBothDirInfo + fileBothDirInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileBothDirInfo = fileBothDirInfo;
			fileBothDirInfo = (PFILE_BOTH_DIR_INFORMATION)((PUCHAR)fileBothDirInfo + fileBothDirInfo->NextEntryOffset);
			if (lastFileBothDirInfo == fileBothDirInfo)
			{
				break;
			}
		}
		break;

	case FileIdBothDirectoryInformation:
		lastFileIdBothDirInfo = NULL;
		fileIdBothDirInfo = (PFILE_ID_BOTH_DIR_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileIdBothDirInfo->FileName;
			fileName.Length = (USHORT)fileIdBothDirInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileIdBothDirInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileIdBothDirInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileIdBothDirInfo->NextEntryOffset += fileIdBothDirInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileIdBothDirInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileIdBothDirInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileIdBothDirInfo = (PFILE_ID_BOTH_DIR_INFORMATION)((PUCHAR)fileIdBothDirInfo + fileIdBothDirInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileIdBothDirInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_BOTH_DIR_INFORMATION, FileName) + nextFileIdBothDirInfo->FileNameLength;
							nextFileIdBothDirInfo = (PFILE_ID_BOTH_DIR_INFORMATION)((PUCHAR)nextFileIdBothDirInfo + nextFileIdBothDirInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_BOTH_DIR_INFORMATION, FileName) + nextFileIdBothDirInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileIdBothDirInfo,
							(PUCHAR)fileIdBothDirInfo + fileIdBothDirInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileIdBothDirInfo = fileIdBothDirInfo;
			fileIdBothDirInfo = (PFILE_ID_BOTH_DIR_INFORMATION)((PUCHAR)fileIdBothDirInfo + fileIdBothDirInfo->NextEntryOffset);
			if (lastFileIdBothDirInfo == fileIdBothDirInfo)
			{
				break;
			}
		}
		break;

	case FileIdFullDirectoryInformation:
		lastFileIdFullDirInfo = NULL;
		fileIdFullDirInfo = (PFILE_ID_FULL_DIR_INFORMATION)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
		for (;;)
		{
			//
			// Create a unicode string from file name so we can use FsRtl
			//
			fileName.Buffer = fileIdFullDirInfo->FileName;
			fileName.Length = (USHORT)fileIdFullDirInfo->FileNameLength;
			fileName.MaximumLength = fileName.Length;

			//
			// Check if this is a match on our hide file name
			//
			if (FsRtlIsNameInExpression(gHideFileName, &fileName, TRUE, NULL))
			{
				//
				// Skip this entry
				//
				if (lastFileIdFullDirInfo != NULL)
				{
					//
					// This is not the first entry
					//
					if (fileIdFullDirInfo->NextEntryOffset != 0)
					{
						//
						// Just point the last info's offset to the next info
						//
						lastFileIdFullDirInfo->NextEntryOffset += fileIdFullDirInfo->NextEntryOffset;
					}
					else
					{
						//
						// This is the last entry
						//
						lastFileIdFullDirInfo->NextEntryOffset = 0;
					}
				}
				else
				{
					//
					// This is the first entry
					//
					if (fileIdFullDirInfo->NextEntryOffset != 0)
					{
						//
						// Calculate the length of the whole list
						//
						nextFileIdFullDirInfo = (PFILE_ID_FULL_DIR_INFORMATION)((PUCHAR)fileIdFullDirInfo + fileIdFullDirInfo->NextEntryOffset);
						moveLength = 0;
						while (nextFileIdFullDirInfo->NextEntryOffset != 0)
						{
							//
							// We use the FIELD_OFFSET macro because FileName is declared as FileName[1] which means that
							// we can't just do sizeof(FILE_DIRECTORY_INFORMATION) + nextFileDirInfo->FileNameLength.
							//
							moveLength += FIELD_OFFSET(FILE_ID_FULL_DIR_INFORMATION, FileName) + nextFileIdFullDirInfo->FileNameLength;
							nextFileIdFullDirInfo = (PFILE_ID_FULL_DIR_INFORMATION)((PUCHAR)nextFileIdFullDirInfo + nextFileIdFullDirInfo->NextEntryOffset);
						}

						//
						// Add the final entry
						//
						moveLength += FIELD_OFFSET(FILE_ID_FULL_DIR_INFORMATION, FileName) + nextFileIdFullDirInfo->FileNameLength;

						//
						// We need to move everything forward.
						// NOTE: RtlMoveMemory (memove) is required for overlapping ranges like this one.
						//
						RtlMoveMemory(
							fileIdFullDirInfo,
							(PUCHAR)fileIdFullDirInfo + fileIdFullDirInfo->NextEntryOffset,
							moveLength);
					}
					else
					{
						//
						// This is the first and last entry, so there's nothing to return
						//
						Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
						return FLT_POSTOP_FINISHED_PROCESSING;
					}
				}
			}

			//
			// Advance to the next directory info
			//
			lastFileIdFullDirInfo = fileIdFullDirInfo;
			fileIdFullDirInfo = (PFILE_ID_FULL_DIR_INFORMATION)((PUCHAR)fileIdFullDirInfo + fileIdFullDirInfo->NextEntryOffset);
			if (lastFileIdFullDirInfo == fileIdFullDirInfo)
			{
				break;
			}
		}
		break;

	default:
		//
		// We shouldn't get a post call for anything else
		//
		NT_ASSERT(FALSE);
		return FLT_POSTOP_FINISHED_PROCESSING;
	}


	return FLT_POSTOP_FINISHED_PROCESSING;
}


FLT_PREOP_CALLBACK_STATUS
FsFilter1PreOperationNoPostOperation(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
/*++

Routine Description:

This routine is a pre-operation dispatch routine for this miniFilter.

This is non-pageable because it could be called on the paging path

Arguments:

Data - Pointer to the filter callbackData that is passed to us.

FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
opaque handles to this filter, instance, its associated volume and
file object.

CompletionContext - The context for the completion routine for this
operation.

Return Value:

The return value is the status of the operation.

--*/
{
	UNREFERENCED_PARAMETER(Data);
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);

	PT_DBG_PRINT(PTDBG_TRACE_ROUTINES,
		("FsFilter1!FsFilter1PreOperationNoPostOperation: Entered\n"));

	// This template code does not do anything with the callbackData, but
	// rather returns FLT_PREOP_SUCCESS_NO_CALLBACK.
	// This passes the request down to the next miniFilter in the chain.

	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}


BOOLEAN
FsFilter1DoRequestOperationStatus(
	_In_ PFLT_CALLBACK_DATA Data
)
/*++

Routine Description:

This identifies those operations we want the operation status for.  These
are typically operations that return STATUS_PENDING as a normal completion
status.

Arguments:

Return Value:

TRUE - If we want the operation status
FALSE - If we don't

--*/
{
	PFLT_IO_PARAMETER_BLOCK iopb = Data->Iopb;

	//
	//  return boolean state based on which operations we are interested in
	//

	return (BOOLEAN)

		//
		//  Check for oplock operations
		//

		(((iopb->MajorFunction == IRP_MJ_FILE_SYSTEM_CONTROL) &&
		((iopb->Parameters.FileSystemControl.Common.FsControlCode == FSCTL_REQUEST_FILTER_OPLOCK) ||
			(iopb->Parameters.FileSystemControl.Common.FsControlCode == FSCTL_REQUEST_BATCH_OPLOCK) ||
			(iopb->Parameters.FileSystemControl.Common.FsControlCode == FSCTL_REQUEST_OPLOCK_LEVEL_1) ||
			(iopb->Parameters.FileSystemControl.Common.FsControlCode == FSCTL_REQUEST_OPLOCK_LEVEL_2)))

			||

			//
			//    Check for directy change notification
			//

			((iopb->MajorFunction == IRP_MJ_DIRECTORY_CONTROL) &&
			(iopb->MinorFunction == IRP_MN_NOTIFY_CHANGE_DIRECTORY))
			);
}

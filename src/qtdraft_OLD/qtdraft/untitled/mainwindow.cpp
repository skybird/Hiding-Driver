#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
	qmList(nullptr)
{
    ui->setupUi(this);
    model = new QDirModel(this);
    model->setReadOnly(false);

    // Tie TreeView with DirModel
        // QTreeView::setModel(QAbstractItemModel * model)
        // Reimplemented from QAbstractItemView::setModel().
	ui->treeView->setModel(model);
	model->setSorting(QDir::DirsFirst|QDir::Name|QDir::IgnoreCase);
    //qmList = new QStringListModel(this);
    //ui->listView->setModel(qmList);
}

MainWindow::~MainWindow()
{
	if (ui)
		delete ui;
	if (model)
		delete model;
	if (qmList)
		delete qmList;
}

void MainWindow::on_treeView_doubleClicked(const QModelIndex &index)
{
    fName = model->fileName(index);
    fPath = model->filePath(index);
    if (fPath==fName)
   List.push_back(fPath);
    else
        List.push_back(fPath+fName);
   ui->listWidget->addItem(fName);
}


void MainWindow::on_pushButton_2_clicked()
{
    if(!selFileIndex.isValid())
        return;
    fName = model->fileName(selFileIndex);
    fPath = model->filePath(selFileIndex);
	if (fPath==fName)
    {
         List.push_back(fPath + fName);
		ui->listWidget->addItem(fPath + fName);
    }
	else
    {
         List.push_back(fPath);
		ui->listWidget->addItem(fPath);
    }
}

void MainWindow::on_treeView_clicked(const QModelIndex &index)
{
    selFileIndex = index;
}

void MainWindow::on_pushButton_3_clicked()
{
    if(!selListIndex.isValid())
        return;
    List.removeAt(selListIndex.row());
    ui->listWidget->removeItemWidget(ui->listWidget->takeItem(selListIndex.row()));
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
    selListIndex = index;
}

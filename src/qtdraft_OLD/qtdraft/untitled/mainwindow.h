#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDirModel>
#include <qstringlistmodel.h>
#include <qlistwidget.h>


#include <Windows.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_treeView_doubleClicked(const QModelIndex &index);

    void on_pushButton_2_clicked();
	void on_treeView_clicked(const QModelIndex &index);

    void on_pushButton_3_clicked();

    void on_listWidget_clicked(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
	QDirModel *model;
	QStringListModel* qmList;
	QStringList List;
	QString fPath, fName;
    QModelIndex selFileIndex,selListIndex;

};

#endif // MAINWINDOW_H
